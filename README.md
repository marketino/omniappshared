# OmniAppShared
Public repo for shared omni app stuff

### Setup for development
- In the package that requires `omniappshared` add the local repository to composer.json:
```json
  "repositories": [
    {
      "type": "path",
      "url": "../relative/path/to/repo/omniappshared"
    }
  ]
```
- and set the version to "@dev":
```json
"require": {
  ...
  "superius/omniappshared": "@dev",
}
```
- run composer update
```shell
$ composer update superius/omniappshared
```

#### Helpful links:
- https://stackoverflow.com/questions/18243197/how-to-extend-the-auth-within-a-package
