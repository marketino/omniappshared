<?php

use Illuminate\Support\Facades\Route;

Route::prefix('dev')->group(function () {
    Route::get('/ping', function () {
        try {
            DB::connection()->getPdo();
            return response()->json(['message' => "Connected successfully to: " . DB::connection()->getDatabaseName()]);
        } catch (\Exception $e) {
            return response()->json(['message' => "Could not connect to the database."], 500);
        }
    });
});
