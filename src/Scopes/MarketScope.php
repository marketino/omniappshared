<?php


namespace Superius\OmniApp\Scopes;

use Auth;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class MarketScope implements Scope
{
    public function apply(Builder $builder, Model $model): void
    {
        $market = Auth::user()?->getMarket();

        if (!$market) {
            throw new \RuntimeException('market is missing in Market scope! ('.$model::class.')');
        }

        $builder->where('market', '=', $market);
    }
}
