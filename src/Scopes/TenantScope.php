<?php

namespace Superius\OmniApp\Scopes;

use Auth;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class TenantScope implements Scope
{
    public function apply(Builder $builder, Model $model): void
    {
        $tenant_id = data_get(Auth::user(), 'tenant_id');

        if (!$tenant_id) {
            throw new \RuntimeException('tid is missing in Tenant scope!');
        }

        $builder->where($model->getTable() . '.tenant_id', '=', $tenant_id);
    }
}
