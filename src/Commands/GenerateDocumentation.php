<?php

namespace Superius\OmniApp\Commands;

use App\Models\User;
use Database\Seeders\ApiDocsSeeder;
use Illuminate\Console\Command;
use PHPOpenSourceSaver\JWTAuth\Facades\JWTAuth;

class GenerateDocumentation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'omniapp:generate-docs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate scribe documentation';


    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(ApiDocsSeeder $seeder): int
    {
        //run api docs seeder
        $user = $seeder->run();
        $token = JWTAuth::fromUser($user);

        //run generating documentation with auth token
        \Safe\system('export SCRIBE_AUTH_KEY=' . $token . ' && php artisan scribe:generate');

        return 0;
    }
}
