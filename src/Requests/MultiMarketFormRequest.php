<?php


namespace Superius\OmniApp\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Superius\OmniApp\Helpers\MarketContext;

class MultiMarketFormRequest extends FormRequest
{
    protected $requestClass;
    public $method;

    public function __construct(array $query = [],
        array $request = [],
        array $attributes = [],
        array $cookies = [],
        array $files = [],
        array $server = [],
        $content = null)
    {

        $market = MarketContext::getMarket();

        $requestClass = data_get($this->marketRequests, $market);
        $this->requestClass = new $requestClass($query, $request, $attributes, $cookies, $files, $server, $content);

    }

    public function rules() {
        return $this->requestClass->rules();
    }
}
