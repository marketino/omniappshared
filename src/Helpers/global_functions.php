<?php

use Superius\OmniApp\Enums\MarketEnum;
use Superius\OmniApp\Helpers\MarketContext;

function isHrMarket(): bool
{
    return MarketContext::getMarket() === MarketEnum::HR->value;
}

function isItMarket(): bool
{
    return MarketContext::getMarket() === MarketEnum::IT->value;
}

function isRsMarket(): bool
{
    return MarketContext::getMarket() === MarketEnum::RS->value;
}

function isSiMarket(): bool
{
    return MarketContext::getMarket() === MarketEnum::SI->value;
}

function isUsMarket(): bool
{
    return MarketContext::getMarket() === MarketEnum::US->value;
}
