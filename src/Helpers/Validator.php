<?php

namespace Superius\OmniApp\Helpers;

use function Safe\preg_match;

class Validator
{
    /**
     * @param $val mixed
     * @return bool
     * @throws \Safe\Exceptions\PcreException
     */
    public static function isUuid($val):bool
    {
        return is_string($val) && preg_match('/^' . Patterns::uuid . '$/', $val) === 1;
    }
}
