<?php

namespace Superius\OmniApp\Helpers;

use Auth;
use Superius\OmniApp\Enums\MarketEnum;

class MarketContext
{
    final public static function getMarket(bool $orDie = true): ?int
    {
        $market = Auth::user()?->getMarket();

        if (!$market && $orDie) {
            throw new \RuntimeException('market not set!');
        }

        return $market;
    }

    final public static function getMarketSlug(): string
    {
        $marketSlug = MarketEnum::from(self::getMarket())->name;
        return strtolower($marketSlug);
    }
}
