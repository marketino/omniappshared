<?php

namespace Superius\OmniApp\Helpers;

use Carbon\Carbon;
use Illuminate\Http\Client\Response;
use Illuminate\Support\Facades\Http;
use Superius\OmniApp\Enums\NextStepTypeEnum;

class NextStepApiHelper
{
    public static function set(
        string $subject,
        string $comment,
        string $tenantId = null,
        string $type = null,
        string $date = null,
    ): Response {
        return Http::withHeaders(self::getHeader())
            ->post(config('omniapp.url.aws_local.nextstep') . '/api/a2a/add-nextstep', [
                'type' => $type ?: NextStepTypeEnum::TODO->value,
                'subject' => $subject,
                'comment' => $comment,
                'tenant_id' => $tenantId,
                'date' => $date ?: Carbon::today()->toDateString(),
            ]);
    }

    /**
     * @return array<string,mixed>
     */
    private static function getHeader(): array
    {
        return [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'Market' => MarketContext::getMarket(),
        ];
    }
}
