<?php

namespace Superius\OmniApp\Helpers;

use Throwable;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class OmniAppExceptionHandler extends ExceptionHandler
{
    public function report(Throwable $exception)
    {
        if ($this->shouldReport($exception) && app()->bound('sentry')) {
            app('sentry')->captureException($exception);
        }

        parent::report($exception);
    }
}
