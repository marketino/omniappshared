<?php


namespace Superius\OmniApp\Models;


use Superius\OmniApp\Models\Traits\HasTenant;

class TenantInsertOnlyModel extends OmniModel
{
    use HasTenant;

    /**
     * @var string[]
     */
    protected $guarded = ['tenant_id'];

    public function delete(){
        throw new \Exception('Model can not be deleted');
    }
}
