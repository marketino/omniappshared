<?php

namespace Superius\OmniApp\Models\Traits;

use Superius\OmniApp\Enums\UserGroupEnum;

trait HasUserGroup
{
    public function isAdmin(): bool
    {
        return data_get($this, 'group') === UserGroupEnum::ADMIN->value;
    }

    public function isUser(): bool
    {
        return data_get($this, 'group') === UserGroupEnum::USER->value;
    }

    public function isSupport(): bool
    {
        return data_get($this, 'group') === UserGroupEnum::SUPPORT->value;
    }
}
