<?php


namespace Superius\OmniApp\Models\Traits;


use Illuminate\Support\Facades\App;
use Superius\OmniApp\Scopes\TenantScope;

trait HasTenant
{
    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {

            //allowing seeders and tests to set tid directly
            if ($model->tenant_id && App::runningInConsole()) {
                return;
            }

            $model->tenant_id = data_get(\Auth::user(), 'tenant_id');

            if (!$model->tenant_id) {
                throw new \RuntimeException('tid is missing in model creating!');
            }
        });
    }

    protected static function booted(): void
    {
        static::addGlobalScope(new TenantScope());
    }
}
