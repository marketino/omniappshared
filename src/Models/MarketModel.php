<?php


namespace Superius\OmniApp\Models;

use Illuminate\Support\Facades\App;
use Superius\OmniApp\Scopes\MarketScope;
use Illuminate\Database\Eloquent\SoftDeletes;

class MarketModel extends OmniModel
{
    use SoftDeletes;

    /**
     * @var string[]
     */
    protected $guarded = ['market'];

    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {

            //allowing seeders and tests to set market directly
            if ($model->market && App::runningInConsole()) {
                return;
            }

            $model->market = \Auth::user()?->getMarket();

            if (!$model->market) {
                throw new \RuntimeException('market is missing in model creating! ('.($model::class).')');
            }
        });
    }

    protected static function booted(): void
    {
        static::addGlobalScope(new MarketScope());
    }
}
