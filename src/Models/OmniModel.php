<?php

namespace Superius\OmniApp\Models;

use Superius\OmniApp\Models\Traits\HasUuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

abstract class OmniModel extends Model
{
    use HasFactory;
    use HasUuid;

    public function getDateFormat(): string
    {
        return 'Y-m-d H:i:s.u';
    }
}
