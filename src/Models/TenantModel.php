<?php


namespace Superius\OmniApp\Models;


use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;
use Superius\OmniApp\Models\Traits\HasTenant;


class TenantModel extends OmniModel implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use SoftDeletes;
    use HasTenant;

    /**
     * @var string[]
     */
    protected $guarded = ['tenant_id'];
}
