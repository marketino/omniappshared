<?php

namespace Superius\OmniApp\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Superius\OmniApp\Models\Traits\HasUserGroup;
use PHPOpenSourceSaver\JWTAuth\Contracts\JWTSubject;

/**
 * @description User model on "sub apps" should be just:
 * class User extends OmniAppUser {}
 */
class OmniAppUser extends Authenticatable implements JWTSubject
{
    use HasFactory;
    use HasUserGroup;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<string>
     */
    protected $fillable = [
        'id',
        'tenant_id',
        'market',
        'group',
        'is_demo'
    ];

    /**
     * Used by Eloquent to get primary key type.
     * UUID Identified as a string.
     *
     * @return string
     */
    public function getKeyType()
    {
        return 'string';
    }


    public function getJWTIdentifier(): string
    {
        return $this->getKey();
    }

    /**
     * @return array<string,string>
     */
    public function getJWTCustomClaims(): array
    {
        return [
            'tid'    => data_get($this, 'tenant_id'),
            'group'  => data_get($this, 'group'),
            'market' => data_get($this, 'market')
        ];
    }

    public function getMarket():int{
        return data_get($this, 'market');
    }

    public function getTenantId()
    {
        return data_get($this,'tenant_id');
    }
}
