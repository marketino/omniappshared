<?php

namespace Superius\OmniApp\Providers;

use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;
use Superius\OmniApp\Helpers\OmniAppExceptionHandler;

class OmniAppServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        //global functions
        require_once __DIR__ . '/../Helpers/global_functions.php';

        //set up sentry reporting
        $this->app->bind(
            ExceptionHandler::class,
            OmniAppExceptionHandler::class
        );
    }

    public function boot()
    {
        $this->registerRoutes();
    }

    protected function registerRoutes(): void
    {
        Route::group([
            'prefix' => 'api',
        ], function () {
            $this->loadRoutesFrom(__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' .
                DIRECTORY_SEPARATOR . 'routes' . DIRECTORY_SEPARATOR . 'api.php');
        });
    }
}
