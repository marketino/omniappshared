<?php

namespace Superius\OmniApp\Enums;

enum UserGroupEnum: string
{
    case ADMIN = 'admin';
    case USER = 'user';
    case SUPPORT = 'support';
}
