<?php

namespace Superius\OmniApp\Enums;

use ArchTech\Enums\Options;

enum PaymentTypeCode: string
{
    use Options;

    case CASH = 'CASH';
    case CARD = 'CARD';
    case CHECK = 'CHECK';
    case MOBILE_MONEY = 'MOBILE_MONEY';
    case WIRE_TRANSFER = 'WIRE_TRANSFER';
    case VOUCHER = 'VOUCHER';
    case OTHER = 'OTHER';
    case ZELLE = 'ZELLE';
    case CASH_APP = 'CASH_APP';
    case APPLE_PAY = 'APPLE_PAY';
    case SAMSUNG_PAY = 'SAMSUNG_PAY';
}
