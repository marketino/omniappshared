<?php

namespace Superius\OmniApp\Enums;

enum MarketEnum: int
{
    case HR = 1;

    case IT = 2;

    case SI = 3;

    case RS = 4;

    case US = 5;
}
