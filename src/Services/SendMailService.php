<?php

namespace Superius\OmniApp\Services;

use Illuminate\Support\Facades\Http;

class SendMailService
{
    /**
     * @param array<string,mixed> $mailData
     */
    public function __construct(array $mailData)
    {
        $sendEmail = Http::withToken(config('omniapp.mail_api_token'))
            ->post(config('omniapp.url.mailapp') . '/api/send', $mailData);

        if ($sendEmail->failed()) {
            abort($sendEmail->status(), 'Cannot send email');
        }
    }
}
