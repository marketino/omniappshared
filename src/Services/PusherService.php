<?php

namespace Superius\OmniApp\Services;

use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\Auth;
use Pusher\ApiErrorException;
use Pusher\Pusher;
use Pusher\PusherException;

class PusherService
{
    /**
     * @param string $channel
     * @param string $event
     * @param array<string, mixed> $data
     * @throws ApiErrorException
     * @throws GuzzleException
     * @throws PusherException
     */
    public static function push(string $channel, string $event, array $data): void
    {
        if (!app()->environment(['local', 'staging', 'production'])) {
            return;
        }

        $tenantId = data_get(Auth::user(), 'tenant_id');

        self::getPusherInstance()->trigger("$tenantId.$channel", $event, $data);
    }

    /**
     * @throws PusherException
     */
    private static function getPusherInstance(): Pusher
    {
        $appId = config('omniapp.pusher.app_id');
        $appKey = config('omniapp.pusher.app_key');
        $appSecret = config('omniapp.pusher.app_secret');
        $appCluster = config('omniapp.pusher.app_cluster');

        return new Pusher($appKey, $appSecret, $appId, ['cluster' => $appCluster]);
    }
}
