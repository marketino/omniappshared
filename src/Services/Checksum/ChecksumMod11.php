<?php

namespace Superius\OmniApp\Services\Checksum;

/**
 * Return the control digit for the MOD 11 checksum
 *
 * **Used for vat number checking in SI**
 * - MOD 11 is calculated by:
 *  1. Assign weights to each digit of the id #. The weights in MOD11 are from 2 through a maximum of 10 beginning
 *     with the low order position in the field.
 *  2. Each digit in the id # is multiplied by its weight
 *  3. The results of the multiplication are added together
 *  4. This product is divided by the modulus number 11
 *  5. The remainder is subtracted from the modulus number 11 giving the check digit
 */
class ChecksumMod11
{
    /**
     * Get the control digit from the number sequence from which to calculate the checksum
     *
     * @param string $sequence
     * @return int
     */
    public static function calculateCheckDigit(string $sequence): int
    {
        $product = collect(str_split($sequence))
            ->map(fn ($c) => (int)$c)
            // NOTE: this is currently valid only for si vat number because of the sequence length and weights and
            //       could possibly not work for other sequences
            ->zip(range(8, 2, -1))
            ->map(fn ($pair) => $pair[0] * $pair[1])
            ->sum();

        return 11 - ($product % 11);
    }

    /**
     * Check if the given sequence is valid with the Mod 11 checksum
     *
     * @param string $sequence 8 Digit sequence
     * @return bool
     */
    public static function isValid(string $sequence): bool
    {
        if (strlen($sequence) !== 8) {
            return false;
        }

        $checkDigit = self::calculateCheckDigit(substr($sequence, 0, 7));
        if ($checkDigit === 10) {
            $checkDigit = 0;
        }

        return ($checkDigit !== 11) && ($checkDigit === (int)$sequence[7]);
    }
}
