<?php

namespace Superius\OmniApp\Services\Checksum;

/**
 * Return the control digit for the ISO 7064 MOD 11, 10 standard
 *
 * **Used for vat number checking in HR and RS**
 * - ISO 7064 (MOD 11, 10) is calculated by:
 *   1. the first digit is added to the number 10
 *   2. divide the obtained sum as an integer (with the remainder) by the number 10; if obtained
 *      the remainder 0 is replaced by the number 10 (this number is the so-called intermediate remainder)
 *   3. the obtained intermediate balance is multiplied by the number 2
 *   4. divide the resulting integer product (with the remainder) by the number 11; this rest
 *      mathematically it cannot be 0 because the result of the previous step is always even number
 *   5. the next digit is added to the remainder in the previous step
 *   6. Repeat steps 2, 3, 4 and 5 until all digits are used up
 *   7. the difference between the number 11 and the rest in the last step is the check digit; if it is
 *      remainder 1 check digit is 0 (11-1=10, and 10 has two digits)
 */
class ChecksumIso7064Mod1110
{
    /**
     * @param string $sequence
     * @return int
     */
    public static function calculateCheckDigit(string $sequence): int
    {
        $vatNumberBaseDigits = str_split($sequence);

        $carry = array_reduce($vatNumberBaseDigits, static function ($carry, $digit) {
            $digitCarry = ((int)$digit + $carry) % 10;
            $digitCarry = $digitCarry ?: 10;
            return (2 * $digitCarry) % 11;
        }, 10);

        return (11 - $carry) % 10;
    }

    /**
     * Check if the given sequence is valid with the ISO 7064 MOD 11, 10 checksum
     *
     * @param string $sequence 9 Digit sequence
     * @return bool
     */
    public static function isValid(string $sequence): bool
    {
        if (strlen($sequence) !== 9) {
            return false;
        }

        $checkDigit = self::calculateCheckDigit(substr($sequence, 0, 8));

        return $checkDigit === (int)$sequence[8];
    }
}
